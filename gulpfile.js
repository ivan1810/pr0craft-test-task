const gulp     = require('gulp'),
      webpack  = require('webpack-stream')
      concat   = require('gulp-concat'),
      cleanCSS = require('gulp-clean-css');

gulp.task('css', function() { 
  return gulp.src('./styles/**/*.css')
    .pipe(concat('bundle.css'))
    .pipe(cleanCSS())
    .pipe(gulp.dest('build/'));  
});

gulp.task('js', function() { 
  return gulp.src('./scripts/entry.js')
    .pipe(webpack({
      output: {
        filename: 'bundle.js'
      },
      module: {
        loaders: [
          { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader", query: {presets: ['es2015', 'react']} }
        ]
      }
    }))
    .pipe(gulp.dest('build/'));
});

gulp.task('watch', function() { 
  gulp.watch('./styles/**/*.css', ['css']);
  gulp.watch('./scripts/**/*.js', ['js']);
});