const specializations = [
  { name: 'Project-manager' },
  { name: 'Trainee project-manager' },
  { name: 'Developer' },
  { name: 'Trainee developer' },
  { name: 'Проект-менеджер' },
  { name: 'Стажер проект-менеджер' },
  { name: 'Разработчик' },
  { name: 'Стажер разработчик' },
];

module.exports = specializations;