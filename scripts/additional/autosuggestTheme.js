const autosuggestTheme = {
  input: 'FormInput',
  suggestionsContainer: 'Dropdown-menu full-width',
  suggestion: 'Dropdown-menu__item',
  suggestionFocused: 'Dropdown-menu__action_highlight'
};

module.exports = autosuggestTheme;