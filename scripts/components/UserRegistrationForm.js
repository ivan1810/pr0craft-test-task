const React               = require('react'),
      Autosuggest         = require('react-autosuggest'),
      ReactTelephoneInput = require('react-telephone-input'),
      specializations     = require('../additional/specializations'),
      theme               = require('../additional/autosuggestTheme');

const { Container, 
        Form, 
        FormRow, 
        FormField, 
        FormInput, 
        FormSelect, 
        Button, 
        Row, 
        Col } = require('elemental');

function doNothing() {
  // This function is used as mock event handler
}

function getSuggestions(value) {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
  if (!inputLength) { return []; }
  const result = specializations.filter(function(spec) {
    return spec.name.toLowerCase().indexOf(inputValue) > -1;
  });
  return result;
}

function getSuggestionValue(suggestion) {
  return suggestion.name;
}

function renderSuggestion(suggestion) {
  return (<span className="Dropdown-menu__action">{suggestion.name}</span>);
}

function shouldRenderSuggestions(value) {
  if (typeof value == 'undefined') { return false; }
  return value.trim().length > 0;
}

const UserRegistrationForm = React.createClass({
  getInitialState: function() {
    return {
      value: '',
      suggestions: []
    };
  },
  render: function() { 
    const inputProps = {
      placeholder: 'Укажите профессию',
      value: this.state.value,
      onChange: this.updateValue
    };
    return (
      <Container>
        <Row>
          <Col lg="1/3">
            &nbsp;
            {/* Yeah, Elemental UI grid doesn't support offsets :( */}
          </Col>
          <Col lg="1/3">
            <p className="text-center">
              <strong>Зарегистрируйтесь</strong> и начните продавать услуги через интернет сегодня
            </p>
            <Form>
              <FormRow>
                <FormField width="one-half" label="Имя">
                  <FormInput name="firstName" placeholder="Укажите имя" />
                </FormField>
                <FormField width="one-half" label="Фамилия">
                  <FormInput name="lastName" placeholder="Укажите фамилию" />
                </FormField>
              </FormRow>
              <FormField label="Профессия">
                <Autosuggest 
                  suggestions={this.state.suggestions} 
                  getSuggestionValue={getSuggestionValue} 
                  onSuggestionsUpdateRequested={this.onSuggestionsUpdateRequested} 
                  shouldRenderSuggestions={shouldRenderSuggestions} 
                  renderSuggestion={renderSuggestion} 
                  inputProps={inputProps} 
                  theme={theme} />
              </FormField>
              <FormField label="Телефон">
                <ReactTelephoneInput
                  defaultCountry="ru"
                  flagsImagePath='images/flags.png'
                  onChange={doNothing}
                  onBlur={doNothing} />
              </FormField>
              <div className="text-center">
                <Button type="primary">Зарегистрироваться</Button>
              </div>
            </Form>
          </Col>
        </Row>
      </Container>
    )
  },
  updateValue(event, { newValue }) {
    this.setState({
      value: newValue
    });
  },
  onSuggestionsUpdateRequested({ value }) {
    this.setState({
      suggestions: getSuggestions(value)
    });
  }
});

module.exports = UserRegistrationForm;