const React                = require('react'),
      ReactDOM             = require('react-dom'),
      UserRegistrationForm = require('./components/UserRegistrationForm');

ReactDOM.render(
  <UserRegistrationForm />, 
  document.getElementById('app')
);